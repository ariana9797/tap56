/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica6tap;

import java.awt.event.ActionListener;
import javax.swing.event.ChangeListener;

/**
 *
 * @author arian
 */
public class PanelImitador extends javax.swing.JPanel {

    /**
     * @return the combo1
     */
    public javax.swing.JComboBox<String> getCombo1() {
        return combo1;
    }

    /**
     * @param combo1 the combo1 to set
     */
    public void setCombo1(javax.swing.JComboBox<String> combo1) {
        this.combo1 = combo1;
    }

    /**
     * @return the combo2
     */
    public javax.swing.JComboBox<String> getCombo2() {
        return combo2;
    }

    /**
     * @param combo2 the combo2 to set
     */
    public void setCombo2(javax.swing.JComboBox<String> combo2) {
        this.combo2 = combo2;
    }

    /**
     * @return the grupoRadio1
     */
    public javax.swing.ButtonGroup getGrupoRadio1() {
        return grupoRadio1;
    }

    /**
     * @param grupoRadio1 the grupoRadio1 to set
     */
    public void setGrupoRadio1(javax.swing.ButtonGroup grupoRadio1) {
        this.grupoRadio1 = grupoRadio1;
    }

    /**
     * @return the grupoRadio2
     */
    public javax.swing.ButtonGroup getGrupoRadio2() {
        return grupoRadio2;
    }

    /**
     * @param grupoRadio2 the grupoRadio2 to set
     */
    public void setGrupoRadio2(javax.swing.ButtonGroup grupoRadio2) {
        this.grupoRadio2 = grupoRadio2;
    }

    /**
     * @return the op1
     */
    public javax.swing.JRadioButton getOp1() {
        return op1;
    }

    /**
     * @param op1 the op1 to set
     */
    public void setOp1(javax.swing.JRadioButton op1) {
        this.op1 = op1;
    }

    /**
     * @return the op2
     */
    public javax.swing.JRadioButton getOp2() {
        return op2;
    }

    /**
     * @param op2 the op2 to set
     */
    public void setOp2(javax.swing.JRadioButton op2) {
        this.op2 = op2;
    }

    /**
     * @return the op3
     */
    public javax.swing.JRadioButton getOp3() {
        return op3;
    }

    /**
     * @param op3 the op3 to set
     */
    public void setOp3(javax.swing.JRadioButton op3) {
        this.op3 = op3;
    }

    /**
     * @return the op4
     */
    public javax.swing.JCheckBox getOp4() {
        return op4;
    }

    /**
     * @param op4 the op4 to set
     */
    public void setOp4(javax.swing.JCheckBox op4) {
        this.op4 = op4;
    }

    /**
     * @return the op5
     */
    public javax.swing.JCheckBox getOp5() {
        return op5;
    }

    /**
     * @param op5 the op5 to set
     */
    public void setOp5(javax.swing.JCheckBox op5) {
        this.op5 = op5;
    }

    /**
     * @return the op6
     */
    public javax.swing.JCheckBox getOp6() {
        return op6;
    }

    /**
     * @param op6 the op6 to set
     */
    public void setOp6(javax.swing.JCheckBox op6) {
        this.op6 = op6;
    }

    /**
     * @return the ope1
     */
    public javax.swing.JRadioButton getOpe1() {
        return ope1;
    }

    /**
     * @param ope1 the ope1 to set
     */
    public void setOpe1(javax.swing.JRadioButton ope1) {
        this.ope1 = ope1;
    }

    /**
     * @return the ope2
     */
    public javax.swing.JRadioButton getOpe2() {
        return ope2;
    }

    /**
     * @param ope2 the ope2 to set
     */
    public void setOpe2(javax.swing.JRadioButton ope2) {
        this.ope2 = ope2;
    }

    /**
     * @return the ope3
     */
    public javax.swing.JRadioButton getOpe3() {
        return ope3;
    }

    /**
     * @param ope3 the ope3 to set
     */
    public void setOpe3(javax.swing.JRadioButton ope3) {
        this.ope3 = ope3;
    }

    /**
     * @return the ope4
     */
    public javax.swing.JCheckBox getOpe4() {
        return ope4;
    }

    /**
     * @param ope4 the ope4 to set
     */
    public void setOpe4(javax.swing.JCheckBox ope4) {
        this.ope4 = ope4;
    }

    /**
     * @return the ope5
     */
    public javax.swing.JCheckBox getOpe5() {
        return ope5;
    }

    /**
     * @param ope5 the ope5 to set
     */
    public void setOpe5(javax.swing.JCheckBox ope5) {
        this.ope5 = ope5;
    }

    /**
     * @return the ope6
     */
    public javax.swing.JCheckBox getOpe6() {
        return ope6;
    }

    /**
     * @param ope6 the ope6 to set
     */
    public void setOpe6(javax.swing.JCheckBox ope6) {
        this.ope6 = ope6;
    }

    /**
     * @return the panelAbajo
     */
    public javax.swing.JPanel getPanelAbajo() {
        return panelAbajo;
    }

    /**
     * @param panelAbajo the panelAbajo to set
     */
    public void setPanelAbajo(javax.swing.JPanel panelAbajo) {
        this.panelAbajo = panelAbajo;
    }

    /**
     * @return the panelArriba
     */
    public javax.swing.JPanel getPanelArriba() {
        return panelArriba;
    }

    /**
     * @param panelArriba the panelArriba to set
     */
    public void setPanelArriba(javax.swing.JPanel panelArriba) {
        this.panelArriba = panelArriba;
    }

    /**
     * @return the texto1
     */
    public javax.swing.JTextField getTexto1() {
        return texto1;
    }

    /**
     * @param texto1 the texto1 to set
     */
    public void setTexto1(javax.swing.JTextField texto1) {
        this.texto1 = texto1;
    }

    /**
     * @return the texto2
     */
    public javax.swing.JTextField getTexto2() {
        return texto2;
    }

    /**
     * @param texto2 the texto2 to set
     */
    public void setTexto2(javax.swing.JTextField texto2) {
        this.texto2 = texto2;
    }

    /**
     * @return the valores1
     */
    public javax.swing.JSpinner getValores1() {
        return valores1;
    }

    /**
     * @param valores1 the valores1 to set
     */
    public void setValores1(javax.swing.JSpinner valores1) {
        this.valores1 = valores1;
    }

    /**
     * @return the valores2
     */
    public javax.swing.JSpinner getValores2() {
        return valores2;
    }

    /**
     * @param valores2 the valores2 to set
     */
    public void setValores2(javax.swing.JSpinner valores2) {
        this.valores2 = valores2;
    }

    /**
     * Creates new form PanelImitador
     */
    public PanelImitador() {
        initComponents();
        grupoRadio1.add(op1);
        grupoRadio1.add(op2);
        grupoRadio1.add(op3);
        grupoRadio2.add(ope1);
        grupoRadio2.add(ope2);
        grupoRadio2.add(ope3);
    }
    public void addEventos(ActionListener oyente){
        this.op1.addActionListener(oyente);
        this.op2.addActionListener(oyente);
        this.op3.addActionListener(oyente);
        this.op4.addActionListener(oyente);
        this.op5.addActionListener(oyente);
        this.op6.addActionListener(oyente);
        this.combo1.addActionListener(oyente);
        this.texto1.addActionListener(oyente);
        
//        this.op1.addActionListener(oyente);
      
    }
    public void addEventos2(ChangeListener oyente){
        this.valores1.addChangeListener(oyente);
    }   /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoRadio1 = new javax.swing.ButtonGroup();
        grupoRadio2 = new javax.swing.ButtonGroup();
        panelArriba = new javax.swing.JPanel();
        op1 = new javax.swing.JRadioButton();
        op4 = new javax.swing.JCheckBox();
        texto1 = new javax.swing.JTextField();
        op2 = new javax.swing.JRadioButton();
        op5 = new javax.swing.JCheckBox();
        combo1 = new javax.swing.JComboBox<>();
        op3 = new javax.swing.JRadioButton();
        op6 = new javax.swing.JCheckBox();
        valores1 = new javax.swing.JSpinner();
        panelAbajo = new javax.swing.JPanel();
        ope1 = new javax.swing.JRadioButton();
        ope4 = new javax.swing.JCheckBox();
        texto2 = new javax.swing.JTextField();
        ope2 = new javax.swing.JRadioButton();
        ope5 = new javax.swing.JCheckBox();
        combo2 = new javax.swing.JComboBox<>();
        ope3 = new javax.swing.JRadioButton();
        ope6 = new javax.swing.JCheckBox();
        valores2 = new javax.swing.JSpinner();

        setLayout(new java.awt.GridLayout(2, 1));

        panelArriba.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Original", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Century Gothic", 1, 12))); // NOI18N
        panelArriba.setLayout(new java.awt.GridLayout(3, 3, 7, 10));

        op1.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        op1.setText("Opción 1");
        op1.setName("opc1"); // NOI18N
        panelArriba.add(op1);

        op4.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        op4.setText("Opción 4");
        op4.setName("opc4"); // NOI18N
        panelArriba.add(op4);

        texto1.setName("tex1"); // NOI18N
        panelArriba.add(texto1);

        op2.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        op2.setText("Opción 2");
        op2.setName("opc2"); // NOI18N
        panelArriba.add(op2);

        op5.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        op5.setText("Opción 5");
        op5.setName("opc5"); // NOI18N
        panelArriba.add(op5);

        combo1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        combo1.setName("comb1"); // NOI18N
        combo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combo1ActionPerformed(evt);
            }
        });
        panelArriba.add(combo1);

        op3.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        op3.setText("Opción 3");
        op3.setName("opc3"); // NOI18N
        panelArriba.add(op3);

        op6.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        op6.setText("Opción 6");
        op6.setName("opc6"); // NOI18N
        panelArriba.add(op6);

        valores1.setName("val1"); // NOI18N
        panelArriba.add(valores1);

        add(panelArriba);

        panelAbajo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Espejo", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Century Gothic", 1, 12))); // NOI18N
        panelAbajo.setLayout(new java.awt.GridLayout(3, 3, 5, 10));

        ope1.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        ope1.setText("Opción 1");
        ope1.setEnabled(false);
        ope1.setName("opc11"); // NOI18N
        panelAbajo.add(ope1);

        ope4.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        ope4.setText("Opción 4");
        ope4.setEnabled(false);
        ope4.setName("opc44"); // NOI18N
        panelAbajo.add(ope4);

        texto2.setEnabled(false);
        texto2.setName("tex2"); // NOI18N
        panelAbajo.add(texto2);

        ope2.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        ope2.setText("Opción 2");
        ope2.setEnabled(false);
        ope2.setName("opc22"); // NOI18N
        panelAbajo.add(ope2);

        ope5.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        ope5.setText("Opción 5");
        ope5.setEnabled(false);
        ope5.setName("opc55"); // NOI18N
        panelAbajo.add(ope5);

        combo2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        combo2.setEnabled(false);
        combo2.setName("comb2"); // NOI18N
        panelAbajo.add(combo2);

        ope3.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        ope3.setText("Opción 3");
        ope3.setEnabled(false);
        ope3.setName("opc33"); // NOI18N
        panelAbajo.add(ope3);

        ope6.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        ope6.setText("Opción 6");
        ope6.setEnabled(false);
        ope6.setName("opc66"); // NOI18N
        panelAbajo.add(ope6);

        valores2.setEnabled(false);
        valores2.setName("val2"); // NOI18N
        panelAbajo.add(valores2);

        add(panelAbajo);
    }// </editor-fold>//GEN-END:initComponents

    private void combo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combo1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_combo1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> combo1;
    private javax.swing.JComboBox<String> combo2;
    private javax.swing.ButtonGroup grupoRadio1;
    private javax.swing.ButtonGroup grupoRadio2;
    private javax.swing.JRadioButton op1;
    private javax.swing.JRadioButton op2;
    private javax.swing.JRadioButton op3;
    private javax.swing.JCheckBox op4;
    private javax.swing.JCheckBox op5;
    private javax.swing.JCheckBox op6;
    private javax.swing.JRadioButton ope1;
    private javax.swing.JRadioButton ope2;
    private javax.swing.JRadioButton ope3;
    private javax.swing.JCheckBox ope4;
    private javax.swing.JCheckBox ope5;
    private javax.swing.JCheckBox ope6;
    private javax.swing.JPanel panelAbajo;
    private javax.swing.JPanel panelArriba;
    private javax.swing.JTextField texto1;
    private javax.swing.JTextField texto2;
    private javax.swing.JSpinner valores1;
    private javax.swing.JSpinner valores2;
    // End of variables declaration//GEN-END:variables
}
