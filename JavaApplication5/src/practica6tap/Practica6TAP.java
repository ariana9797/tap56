/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica6tap;

import javax.swing.JFrame;
import javax.swing.event.ChangeListener;

/**
 *
 * @author arian
 */
public class Practica6TAP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        JFrame f = new JFrame("Imitador");
        PanelImitador panel = new PanelImitador();
        Oyente oyente = new Oyente(panel);//OYENTE
        panel.addEventos(oyente); //REGISTRO
        panel.addEventos2( oyente);
        f.setSize(500,300);
        f.setLocation(50,50);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(panel);
        f.setVisible(true);
    }
    
}
