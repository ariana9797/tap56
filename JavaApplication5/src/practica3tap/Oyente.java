/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica3tap;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author arian
 */
public class Oyente implements ActionListener{
    PanelFichero panel;
    Oyente(PanelFichero panel){
        this.panel = panel;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
       
        JFileChooser seleccionado = new JFileChooser(".");
        if(seleccionado.showOpenDialog(panel) == JFileChooser.APPROVE_OPTION){
            File fichero=seleccionado.getSelectedFile();
            //Ecribe la ruta del fichero seleccionado en el campo de texto
            panel.getRuta().setText(fichero.getAbsolutePath());
        }
    }
    
}
