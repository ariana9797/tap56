/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica3tap;

import javax.swing.JFrame;

/**
 *
 * @author arian
 */
public class Practica3TAP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        JFrame f = new JFrame("Mostrar ruta fichero");
        PanelFichero panel = new PanelFichero();
        Oyente oyente = new Oyente(panel);//OYENTE
        panel.addEventos(oyente); //REGISTRO
        f.setSize(700,200);
        f.setLocation(300,100);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(panel);
        f.setVisible(true);
    }
    
}
