/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica4tap;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author arian
 */
public class Oyente implements ActionListener{
    PanelPeliculas panel;
    Oyente(PanelPeliculas panel){
        this.panel =panel;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String peli = panel.getTituloPelicula().getText();
        panel.getPeliculas().addItem(peli);
        panel.getTituloPelicula().setText("");
    }
    
}
