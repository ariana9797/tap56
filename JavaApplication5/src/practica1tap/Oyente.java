/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1tap;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author arian
 */
public class Oyente implements ActionListener{
    PanelSaludo panel;

    Oyente(PanelSaludo panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String nombre = panel.getNombre().getText();
        JOptionPane.showMessageDialog(null, "¡Hola "+ nombre+"!");
        panel.getNombre().setText("");
    }
    
    
    
}
