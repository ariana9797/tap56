/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1tap;

import javax.swing.JFrame;

/**
 *
 * @author arian
 */
public class Practica1TAP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        JFrame f = new JFrame("Saludador");
        PanelSaludo panel = new PanelSaludo();
        Oyente oyente = new Oyente(panel);//OYENTE
        panel.addEventos(oyente); //REGISTRO
        f.setSize(500,300);
        f.setLocation(50,50);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(panel);
        f.setVisible(true);
        
    }
    
}
