/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2tap;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;


/**
 *
 * @author arian
 */
public class Oyente implements ActionListener{
    PanelNumeros panel;
    
    Oyente(PanelNumeros panel){
        this.panel = panel;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        int num1 = (int) panel.getNumero1().getValue();
        int num2 = (int) panel.getNumero2().getValue();
        Random rnd = new Random();
        String res = Integer.toString((int)(Math.random()* (num2-num1+1) + num1));//Integer.toString((int)(Math.random()* num2 + num1));
        panel.getResultado().setText(res);
      //  System.out.println(res);
    }
    
}
